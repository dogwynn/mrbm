# https://stackoverflow.com/a/19706670/1869370
import http.server
import ssl

httpd = http.server.HTTPServer(
    ('0.0.0.0', 4443),
    http.server.SimpleHTTPRequestHandler
)
httpd.socket = ssl.wrap_socket(
    httpd.socket,
    certfile='/Users/dogwynn/projects/mrbm/cert.pem',
    keyfile='/Users/dogwynn/projects/mrbm/key.pem',
    server_side=True,
    ssl_version=ssl.PROTOCOL_TLSv1
)
httpd.serve_forever()
