#!/usr/bin/env bash
# https://stackoverflow.com/a/42298344/1869370
openssl req -x509 -newkey rsa:2048 -keyout keytmp.pem -out cert.pem -days 365
openssl rsa -in keytmp.pem -out key.pem

