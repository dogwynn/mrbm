#!/usr/bin/env python3

import os
import zipfile
import argparse
import itertools
import warnings
from xml.etree import ElementTree as etree
from functools import wraps, lru_cache
from pathlib import Path
import urllib.parse


import numpy as np
from fastkml import kml
import fastkml.styles as kml_styles
from shapely.geometry import (
    Point, Polygon, MultiPoint, LineString, MultiPolygon,
)
from pyquaternion import Quaternion
import geopy

memoize = lru_cache(None)
slerp = Quaternion.slerp


def memoized_property(fget):
    """Return a property attribute for new-style classes that only calls
    its getter on the first access. The result is stored and on
    subsequent accesses is returned, preventing the need to call the
    getter any more.

    Example::
        >>> class C(object):
        ...     load_name_count = 0
        ...     @memoized_property
        ...     def name(self):
        ...         "name's docstring"
        ...         self.load_name_count += 1
        ...         return "the name"
        >>> c = C()
        >>> c.load_name_count
        0
        >>> c.name
        "the name"
        >>> c.load_name_count
        1
        >>> c.name
        "the name"
        >>> c.load_name_count
        1

    """
    attr_name = '_{0}'.format(fget.__name__)

    @wraps(fget)
    def fget_memoized(self):
        if not hasattr(self, attr_name):
            setattr(self, attr_name, fget(self))
        return getattr(self, attr_name)

    return property(fget_memoized)

class KMLParserError(Exception):
    pass

class KMLParser:
    def __init__(self, path):
        self.path = path
        self.check_extension()
        self.set_ns()
    
    def check_extension(self):
        _, ext = os.path.splitext(self.path)
        if ext not in {'.kmz','.kml'}:
            raise IOError("Don't know how to open file with extension: {}".format(ext))

    def fp(self):
        _, ext = os.path.splitext(self.path)
        if ext == '.kmz':
            zf = zipfile.ZipFile(self.path)
            potentials = [z.filename
                          for z in zf.filelist if z.filename.endswith('.kml')]
            if len(potentials)==1:
                filename = potentials[0]
                fp = zipfile.ZipFile(self.path).open(filename,'r')
            else:
                raise KMLParserError('ambiguous internal KML')
        else:
            fp = open(self.path,'r')
        return fp

    def set_ns(self):
        ns = {}
        with self.fp() as fp:
            for event, elem in etree.iterparse(fp, ('start-ns',)):
                if event == 'start-ns':
                    key, uri = elem
                    if not key:
                        key = 'kml'
                    ns[key] = uri
        self.ns = ns

    @memoized_property
    def document(self):
        with self.fp() as fp:
            tree = etree.parse(fp)

        root = tree.getroot()
        return root.find('kml:Document',self.ns)

    def placemarks(self, path='Folder/Placemark'):
        # with self.fp() as fp:
        #     for event, elem in etree.iterparse(fp, ('start', 'end')):
        #         if event == 'start':
        #             yield elem
        #             break
        for placemark in self.iterfind(self.document, path):
            yield placemark

    def cities(self, *, condition=None, placemark_path='Folder/Placemark',
               all_ms=True):
        mg_coord_path = (
            'MultiGeometry/Polygon/outerBoundaryIs/LinearRing/coordinates'
        )
        p_coord_path = (
            'Polygon/outerBoundaryIs/LinearRing/coordinates'
        )
        for pm in self.placemarks():
            data = {}
            for d in self.findall(pm, 'ExtendedData/SchemaData/SimpleData'):
                data[d.attrib['name']] = d.text
            if not condition or condition(data, all_ms=all_ms):
                polygons = []
                for c in itertools.chain(self.findall(pm, mg_coord_path),
                                         self.findall(pm, p_coord_path)):
                    polygons.append(
                        MultiPoint([tuple(float(v) for v in p.split(','))
                                    for p in c.text.split()]).convex_hull
                    )
                data['polygons'] = polygons
                yield data
                    
    def find(self, E, path):
        path = '/'.join([p if ':' in p else 'kml:'+p for p in path.split('/')])
        try:
            return E.find(path,self.ns)
        except:
            warnings.warn(
                'Tried to find path: {} with namespace: {} and failed'.format(
                    path, self.ns
                )
            )
            return []

    def iterfind(self, E, path):
        path = '/'.join([p if ':' in p else 'kml:'+p for p in path.split('/')])
        for element in E.iterfind(path, self.ns):
            yield element

    def findall(self, E, path):
        path = '/'.join([p if ':' in p else 'kml:'+p for p in path.split('/')])
        try:
            return E.findall(path,self.ns)
        except:
            warnings.warn(
                'Tried to find path: {} with namespace: {} and failed'.format(
                    path, self.ns
                )
            )
            return []


def normalize(v, tolerance=0.00001):
    mag2 = (v**2).sum()
    if abs(mag2 - 1.0) > tolerance:
        mag = np.sqrt(mag2)
        v /= mag
    return v

class LatLong:
    def __init__(self, lon, lat, height=0, *, miles=False):
        self.ll = np.array([lat, lon])
        self.rads = np.radians(self.ll)
        self.h = 1.609344 * height if miles else height

    @classmethod
    def from_quat(cls, quat):
        return cls.from_xyz(quat.vector)

    @classmethod
    def from_xyz(cls, xyz):
        x, y, z = normalize(xyz)
        lat = np.degrees(np.arcsin(z))
        lon = np.degrees(np.arctan2(y, x))
        return cls(lon, lat)

    @memoized_property
    def xyz(self):
        x = np.cos(self.rads[0]) * np.cos(self.rads[1])
        y = np.cos(self.rads[0]) * np.sin(self.rads[1])
        z = np.sin(self.rads[0])
        return np.array([x, y, z])

    @memoized_property
    def quat(self):
        return Quaternion(vector=self.xyz)

    def rotation(self, theta):
        return Quaternion(axis=self.xyz, angle=np.radians(theta))

    def rotate(self, other, theta=None):
        if theta is not None:
            # other is a LatLong
            other = other.rotation(theta)
        return LatLong.from_xyz(other.rotate(self.xyz))

    def scale(self, other, factor):
        return LatLong.from_quat(slerp(self.quat, other.quat, 1 - factor))

    def translation(self, other):
        axis = np.cross(self.xyz, other.xyz)
        angle = np.linalg.norm(axis)
        return Quaternion(axis=axis, angle=angle)

    def translate(self, quat):
        return self.rotate(quat)
    

@memoize
def geocode(city):
    coder = geopy.geocoders.GoogleV3()
    loc = coder.geocode(city)
    return loc.latitude, loc.longitude


@memoize
def orig_river_kml():
    k = kml.KML()
    k.from_string(Path('./orig_msriver.kml').read_bytes())
    return k


@memoize
def high_res_cities_kml():
    k = kml.KML()
    k.from_string(Path('./cb_2016_us_ua10_500k.kml').read_bytes())
    return k


@memoize
def bounds_kml():
    k = kml.KML()
    k.from_string(Path('./MRBM-bounds.kml').read_bytes())
    return k


@memoize
def get_tamale_kml():
    k = kml.KML()
    k.from_string(Path('./hot-tamale-trail-map.kml').read_bytes())
    return k


@memoize
def get_bounds():
    D = list(bounds_kml().features())[0]
    placemark = list(D.features())[0]
    return Polygon(placemark.geometry.exterior.coords)


@memoize
def get_mr3():
    D = list(orig_river_kml().features())[0]
    F = list(D.features())[0]
    placemark = list(F.features())[-1]
    return LineString(placemark.geometry.coords)


@memoize
def get_bounded_river():
    mr3 = get_mr3()
    bounds = get_bounds()
    return mr3.intersection(bounds)


@memoize
def get_blues_trail_kml():
    k = kml.KML()
    k.from_string(Path('./mississippi-blues-trail.kml').read_bytes())
    return k


def large_enough(data, size=6e7):
    return int(data['ALAND10']) > size


def condition(data, all_ms=True):
    if data['NAME10'].split(',')[-1].strip()=='MS' and all_ms:
        return True
    if large_enough(data):
        return True
    return False


def river_cities(all_ms=True):
    bounds = get_bounds()
    kp = KMLParser('cities.kml')
    for city in kp.cities(condition=condition,all_ms=all_ms):
        if any(bounds.intersects(p) for p in city['polygons']):
            yield city


def get_tamale_data():
    k = get_tamale_kml()
    placemarks = [pm for D in k.features()
                  for F in D.features()
                  for pm in F.features()]
    return placemarks


KMLNS = '{http://www.opengis.net/kml/2.2}'
RP = (-91.10336290125692, 32.54951845444171)
RPLL = LatLong(*RP)
IP = (-90.31726083796993, 32.306660835405744)
IP = (-90.07234621298656, 32.45473507367424)  # my house
IP = (-90.17042612459966,32.31852557758678)  # belhaven
IPLL = LatLong(*IP)
ANGLE = (-102.6)
SCALE = 1 / 2000
# SCALE = 1 / 10000
TR_Q = RPLL.translation(IPLL)

WRLD_API_KEY = 'bf669b9c6975f5bccf1b1a252bd5a044'
WRLD_DEV_TOKEN = 'db9efa7ef2b54a58f95ccde7a20817e32ed810d9e1a9aa7786de12156b4a5b567026b029c4f7e6e6'

def transform_ll(ll):
    return ll.rotate(RPLL, ANGLE).scale(RPLL, SCALE).translate(TR_Q)
    # return ll.translate(TR_Q).rotate(RPLL, ANGLE).scale(RPLL, SCALE)
    # return ll.scale(RPLL, SCALE).rotate(RPLL, ANGLE)#.translate(TR_Q)


def transform_poly(polygon):
    return Polygon(
        [transform_ll(LatLong(*c)).ll[::-1] for c in polygon.exterior.coords]
    )


def transform_ls(linestring):
    return LineString(
        [transform_ll(LatLong(*c)).ll[::-1] for c in linestring.coords]
    )


def transform_point(point):
    return Point(
        transform_ll(LatLong(*point.coords[0])).ll[::-1]
    )


def transform(feature):
    tfunc = {
        LineString: transform_ls,
        Polygon: transform_poly,
        Point: transform_point,
    }.get(feature.__class__)

    if not tfunc:
        raise NotImplementedError(
            'no transform for this type of object:'
            ' {}'.format(feature.__class__)
        )

    return tfunc(feature)


def maps_url_ll(lat, lon):
    return maps_url_q('loc:{}+{}'.format(lat, lon))


def maps_url_q(q):
    return 'http://maps.google.com/maps?q={}'.format(urllib.parse.quote(q))


def build_city_folder(transform=transform, all_ms=True):
    pstyle_id = 'city-styles'
    styles = [
        kml_styles.PolyStyle(
            color='7fe1ca9e',
        ),
        kml_styles.IconStyle(
            scale=0.7,
            icon_href='https://maps.google.com/mapfiles/kml/pal4/icon57.png',
        ),
    ]
    S = kml.Style(KMLNS, pstyle_id)
    for s in styles:
        S.append_style(s)

    F = kml.Folder(KMLNS, 'ms-river-cities', 'Mississippi River Basin Cities')

    # D.append(F)
    sf_lut = {}
    for i, city in enumerate(river_cities(all_ms=all_ms)):
        id = '-'.join(
            itertools.chain(*[p.split() for p in city['NAME10'].split(',')])
        )
        # state_abrev = city['NAME10'].split(',')[-1].split('--')[0].strip()
        # if state_abrev not in sf_lut:
        #     sf_lut[state_abrev] = SF = kml.Folder(
        #         KMLNS, state_abrev, state_abrev,
        #     )
        #     F.append(SF)
        # SF = sf_lut[state_abrev]

        def city_name(c):
            whole = city['NAME10']
            name, state = [s.strip() for s in whole.split(',')]
            state = state.split('--')[0]
            return '{}, {}'.format(name, state)
        
        # CF = kml.Folder(KMLNS, id, city['NAME10'])
        # SF.append(CF)

        polygon = MultiPolygon(city['polygons']).convex_hull
        if transform is not None:
            polygon = transform(polygon)

        if large_enough(city, 8e7):
            pm = kml.Placemark(
                KMLNS, id, city['NAME10'],
                description='_',
                # description=(
                #     '<a href="{}">Actual City [Google Maps]</a>'.format(
                #         maps_url_q(city_name(city))
                #     )
                # ),
                styleUrl='#' + pstyle_id,
            )
            # polygon = MultiPolygon(city['polygons']).convex_hull
            # if transform is not None:
            #     polygon = transform(polygon)
            pm.geometry = polygon
            F.append(pm)
        # CF.append(pm)
        # SF.append(pm)

        npm = kml.Placemark(
            KMLNS, id, city['NAME10'],
            styleUrl='#' + pstyle_id,
        )
        npm.geometry = Point(polygon.centroid)
        # CF.append(npm)
        # SF.append(npm)

        F.append(npm)

    # for s_abbrev in sorted(sf_lut):
    #     F.append(sf_lut[s_abbrev])

    return F, [S]


def build_river_folder(transform=transform):
    pstyle_id = 'default-river-polystyle'
    styles = [
        kml_styles.PolyStyle(
            color='52ee9e9e',
        ),
    ]
    S = kml.Style(KMLNS, pstyle_id)
    for s in styles:
        S.append_style(s)

    F = kml.Folder(KMLNS, 'ms-river', 'Mississippi River', ' ')
    river = get_bounded_river()

    polygon = river.buffer(0.015).buffer(-0.005)

    pm = kml.Placemark(
        KMLNS, 'ms-river-polygon', 'Mississippi River',
        styleUrl='#' + pstyle_id,
    )
    if transform is not None:
        polygon = transform(polygon)
    pm.geometry = polygon
    F.append(pm)

    return F, [S]


def build_hot_tamale_folder(transform=transform):
    pstyle_id = 'hot-tamale-trail'
    icon_style = kml_styles.IconStyle(
        scale=0.7,
        icon_href=('https://belhavencs.nyc3.digitaloceanspaces.com'
                   '/MRBM/tamale-icon.png'),
    )
    styles = [
        icon_style,
    ]
    S = kml.Style(KMLNS, pstyle_id)
    for s in styles:
        S.append_style(s)

    tamale_data = get_tamale_data()

    F = kml.Folder(
        KMLNS, 'hot-tamale-trail', 'Hot Tamale Trail Map',
        ('Welcome to the map for the Mississippi Delta Hot'
         ' Tamale Trail, a project of the Southern Foodways'
         ' Alliance. We created the Tamale Trail in an effort'
         ' to document the history, tradition, and culture of'
         ' hot tamales from Tunica to Vicksburg, offering'
         ' cultural tourists a complete guide to a unique'
         ' culinary experience. So head to the Mississippi Delta,'
         ' which has been called “the most Southern place on'
         ' earth” for its unique history, music, culture, and'
         ' yes, its food.<br><br>Grab a shuck and go!<br><br>'
         'www.tamaletrail.com<br>www.southernfoodways.org<br>')
    )

    for pm in tamale_data:
        pm.geometry = transform(pm.geometry)
        pm.styleUrl = '#' + pstyle_id
        F.append(pm)

    return F, [S]


def build_mrbm_kml(all_ms=True):
    k = kml.KML()
    
    D = kml.Document(
        KMLNS, 'mrbm-model', 'Mississippi River Basin Model', ' ',
    )

    builder_funcs = [
        build_river_folder,
        build_hot_tamale_folder,
        lambda:build_city_folder(all_ms=all_ms),
    ]
    folders, styles = zip(*[f() for f in builder_funcs])

    for S in itertools.chain(*styles):
        D.append_style(S)

    for F in folders:
        D.append(F)

    k.append(D)
    return k

        
def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output', default='mrbm.kml')
    parser.add_argument('--not-all-ms', action='store_false')

    args = parser.parse_args()
    return args
    
def main():
    # orig = orig_river_kml()
    # mr3 = get_mr3(orig)
    
    # new = new_river_kml()

    args = get_args()

    kml = build_mrbm_kml(all_ms=args.not_all_ms)
    path = Path(args.output).expanduser().resolve()
    path.write_text(kml.to_string(prettyprint=True))
    
    

if __name__=='__main__':
    main()
